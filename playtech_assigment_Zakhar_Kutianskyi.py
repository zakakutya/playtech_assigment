import requests
import tkinter as tk 
from tkinter import ttk
CURRENCY_NAMES = ['CHF', 'EUR', 'GBP', 'JPY', 'PLN', 'QAR', 'SEK', 'TRY', 'UAH', 'USD']
cache= {}                                                           #Create a cache to store the values 


#Function that checks if the input is a number
def check_is_digit(input):
    if input.isdigit():
        return True
    else:
        try:
            float(input)
            return True
        except ValueError:
            return False


#Function that formats the values
def format_currency_output (value):
    "{:.6f}".format(value)
    return value


#Function that converts the values
def convert():
    clear_table()
    input_value = input_entry.get()                                 #Gets the unput value
    from_currency = from_dropdown.get()                             #Gets the currency to convert from
    to_currency = to_dropdown.get()                                 #Gets the currency to convert to
    
    if from_currency == to_currency:                                #Checks if the currencies are the same
        result_label.config(text=f"The currencies are the same. No conversion needed.")
        result_label.pack()
        return

    if check_is_digit(input_value):                                 #Checks if the input value is a number
        # Get the profit, amount of intermediary currency, the amount of the final currency for each intermediary currency and the direct conversion
        profit, from_currency_to_inter, converted_amount, direct_converted_amount = currency_converter(from_currency, to_currency, input_value)
        
        # Add the values to the table
        for i in CURRENCY_NAMES:
            
            if i == from_currency or i == to_currency:
                continue
            else:
                table.insert("", tk.END, values=(i, str(format_currency_output(from_currency_to_inter[i])),
                                                    str(format_currency_output(converted_amount[i])),
                                                    str(format_currency_output(profit[i]))))

        #Sort the profit dictionary to get the best intermediary currency for the customer and the service provider
        best_for_customer = max(profit, key=lambda k: profit[k])
        best_for_service = min(profit, key=lambda k: profit[k])
        # Update the label with the value
        result_label.config(text=f"{input_value} {from_currency} = {direct_converted_amount} {to_currency}")
        best_for_service_label.config(text=f"Best for customer: {best_for_service}")
        best_for_customer_label.config(text=f"Best for service provider: {best_for_customer}")  
    else:
        result_label.config(text=f"Please enter a valid number")
        result_label.pack()


#Function that clear the table
def clear_table():
    table.delete(*table.get_children())


#Function that gets the values from the API
def api_request(currency):
    url = f'https://api.exchangerate.host/latest?base={currency}&symbols={CURRENCY_NAMES}'
    response = requests.get(url)
    data = response.json()
    return data['rates']


#Function that converts the currencies
def currency_converter(from_currency, to_currency, input_amount):
    if (from_currency == to_currency):                              #Checks if the currencies are the same
        print("The currencies are the same. No conversion needed.")
        return
    if not input_amount:                                            #Checks if the input value is empty
        print("The input amount is not valid.")
        return
    try:
        input_amount = float(input_amount)                          #Converts the input value to float
    except ValueError:
        result_label.config(text=f"Please enter a valid number")
        result_label.pack()
        return
    
    from_currency_to_inter = {}   #Create empty dictionaries
    converted_amount = {}
    profit = {}
    if from_currency not in cache :                                                        #Checks if the currency is in the cache
        cache[from_currency] = api_request(from_currency)                                  #Gets the currency from the API
    direct_rate = cache[from_currency][to_currency]                                        #Gets the direct rate from the cache
    direct_converted_amount = float(round(input_amount * direct_rate,  6))                 #Calculates the direct converted amount
    for inter_currency in CURRENCY_NAMES:                                                                                 
        if inter_currency != from_currency and inter_currency != to_currency:              #Checks if the currency is not the same as the from and to currencies
            if inter_currency not in cache:                                                #Checks if the intermediary currency is in the cache
                cache[inter_currency] = api_request(inter_currency)                        #Gets the intermediary currency from the API
            base_to_inter_rate = cache[from_currency][inter_currency]                                   #Get the rates from the cache
            inter_to_taget_rate = cache[inter_currency][to_currency]   
            
            
            from_currency_to_inter[inter_currency] = float(round(base_to_inter_rate * input_amount))                 #Calculates the amount to be converted to the intermediary currency
            converted_amount[inter_currency] = float(round(from_currency_to_inter[inter_currency] * inter_to_taget_rate, 6))  #Calculates the amount to be converted to the to currency
            profit[inter_currency] = float(round(direct_converted_amount - converted_amount[inter_currency], 6)) #Calculates the profit
            
            
    return profit, from_currency_to_inter, converted_amount, direct_converted_amount        #Returns the neccessary values

# Create a Tkinter window
root = tk.Tk()
root.title("Profitability of Intermediary Currencies")

# Create a frame for the dropdown menus and input field
input_frame = tk.Frame(root)

# Create a dropdown menu for the "from" currency
from_label = tk.Label(input_frame, text="From:")
from_label.pack(side=tk.LEFT)
from_dropdown = ttk.Combobox(input_frame, values = CURRENCY_NAMES)
from_dropdown.pack(side=tk.LEFT)
from_dropdown.current(0)

# Create a dropdown menu for the "to" currency
to_label = tk.Label(input_frame, text="To:")
to_label.pack(side=tk.LEFT)
to_dropdown = ttk.Combobox(input_frame, values = CURRENCY_NAMES)
to_dropdown.pack(side=tk.LEFT)
to_dropdown.current(1)

# Create an input field for the value to convert
input_label = tk.Label(root, text="Enter a value:")
input_label.pack()
input_entry = tk.Entry(root)
input_entry.pack()

# Create a button to convert
convert_button = tk.Button(root, text="Convert", command=convert)
convert_button.pack()

# Create a button to clear the table
clear_button = tk.Button(root, text="Clear", command=clear_table)
clear_button.pack()

# Create a frame for the result table
result_frame = tk.Frame(root)

# Create a label to display the converted value
result_label = tk.Label(result_frame, text="")
best_for_service_label = tk.Label(result_frame, text="")
best_for_customer_label = tk.Label(result_frame, text="")
result_label.pack()
best_for_service_label.pack()
best_for_customer_label.pack()

# Create a table to display the conversion history
table_frame = tk.Frame(root)
table_frame.pack(pady=5)
table = ttk.Treeview(table_frame, columns=["Inter currency", "To Inter currency", "From Inter currency", "Profit"])
table.heading("Inter currency", text="Inter currency")
table.heading("To Inter currency", text="To Inter currency")
table.heading("From Inter currency", text="From Inter currency")
table.heading("Profit", text="Profit")

table.pack()

# Pack the input frame and result frame into the root window
input_frame.pack()
result_frame.pack()
table_frame.pack()


root.mainloop() # Start the GUI